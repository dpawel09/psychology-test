package com.teamway.app.personalitytest;

import com.teamway.app.personalitytest.controller.QuestionController;
import com.teamway.app.personalitytest.model.Answer;
import com.teamway.app.personalitytest.model.Question;
import com.teamway.app.personalitytest.model.Quiz;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

@SpringBootTest
class PersonalityTestApplicationTests {

	private static final String TEST_QUESTION_1 = "testQuestion1";
	private static final String TEST_ANSWER_1 = "testAnswer1";

	@Autowired
	private QuestionController questionController;

	@Test
	void contextLoads() {
	}

	@Test
	@Transactional
	void saveAndGet() {
		// GIVEN
		final Quiz quiz = createQuiz();


		// WHEN
		questionController.save(quiz);

		// THEN
		final Quiz result = questionController.getAll().getBody();

		assertThat(result).isNotNull();
		assertThat(result.getQuestions().size()).isEqualTo(1);
		assertThat(result.getQuestions().get(0).getQuestion()).isEqualTo(TEST_QUESTION_1);
		assertThat(result.getQuestions().get(0).getAnswers().size()).isEqualTo(1);
		assertThat(result.getQuestions().get(0).getAnswers().get(0).getAnswer()).isEqualTo(TEST_ANSWER_1);
	}

	private Quiz createQuiz() {
		final Question question = new Question();
		question.setQuestion(TEST_QUESTION_1);

		final Answer answer = new Answer(null, TEST_ANSWER_1, 1, question);

		question.setAnswers(List.of(answer));

		return new Quiz(List.of(question));
	}
}
