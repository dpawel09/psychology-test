import * as React from 'react';
import './Header.css';

const Header = (): JSX.Element => {
    return (
        <header>
            Personality test. Are you introvert or extravert?
        </header>
    )
}

export default Header;
