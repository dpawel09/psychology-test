import * as React from 'react';
import * as renderer from 'react-test-renderer';
import { Store } from '../../store/Store';
import { QuestionComponent, mapStateToProps, Props } from './Question';

describe('Question', () => {
    
    test('snapshot', () => {
        // given
        const props = {
            question: {
                question: 'test question',
                answers: [
                    {
                        answer: 'test answer 1',
                        score: 2,
                    },
                    {
                        answer: 'test answer 2',
                        score: 3,
                    },
                ],
            },
            currentQuestionNumber: 1,
            totalQuestionNumber: 2,
        } as Props;

        // when
        const rendered = renderer.create(<QuestionComponent {...props} />).toJSON();

        // then
        expect(rendered).toMatchSnapshot();
    });

    test('mapStateToProps', () => {
        // given
        const state = {
            quiz: {
                questions: [
                    { question: 'question 1' },
                    { question: 'question 2' },
                ],
                currentQuestionIndex: 1,
            }
        } as Store;

        // when
        const result = mapStateToProps(state);

        // then
        expect(result.currentQuestionNumber).toBe(2);
        expect(result.totalQuestionNumber).toBe(2);
        expect(result.currentQuestionNumber).toBe(2);
    });
});
