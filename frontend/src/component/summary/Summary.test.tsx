import * as React from 'react';
import * as renderer from 'react-test-renderer';
import { Store } from '../../store/Store';
import { SummaryComponent, isExtravert, mapStateToProps } from './Summary';

describe('Summary', () => {

    test('snapshot', () => {
        // given
        const props = {
            maximumScore: 4,
            score: 2
        };

        // when
        const rendered = renderer.create(<SummaryComponent {...props} />).toJSON();

        // then
        expect(rendered).toMatchSnapshot();
    });

    test('isExtravert then false', () => {
        // when
        const result = isExtravert({ maximumScore: 4, score: 1 });

        // then
        expect(result).toBe(false);
    });

    test('isExtravert then true', () => {
        // when
        const result = isExtravert({ maximumScore: 4, score: 2 });

        // then
        expect(result).toBe(true);
    });

    test('mapStateToProps', () => {
        // given
        const state = {
            quiz: {
                questions: [{}, {}],
                score: 4,
            }
        } as Store;
        
        // when
        const result = mapStateToProps(state);

        // then
        expect(result.maximumScore).toBe(8);
        expect(result.score).toBe(4);
    });
});
