import * as React from 'react';
import { connect } from 'react-redux';
import { Store } from '../../store/Store';
import './Summary.css';

interface StateProps {
    maximumScore: number,
    score: number,
}

const SummaryComponent = (props: StateProps): JSX.Element => {
    return (
        <div className="summary">
            <p>Test summary</p>
            <p>You are more of an {isExtravert(props) ? 'extravert' : 'introvert'}.</p>
        </div>
    )
}

const isExtravert = (props: StateProps): boolean => {
    return (props.score / props.maximumScore) >= 0.5;
}

const mapStateToProps = (state: Store): StateProps => {
    return {
        maximumScore: state.quiz.questions.length * 4,
        score: state.quiz.score,
    }
}

const Summary = connect(mapStateToProps)(SummaryComponent);

export {
    Summary,
    SummaryComponent,
    isExtravert,
    mapStateToProps
}
