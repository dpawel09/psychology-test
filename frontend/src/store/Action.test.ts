import { createInit, Action, ActionType, createSelectAnswer } from "./Action";
import { Quiz } from "./Store";

describe('Action test', () => {

    test('createInit', () => {
        // given
        const data = {
            questions: [{}],
        } as Quiz;

        // when
        const result = createInit(data);

        // then
        expect(result.type).toBe(ActionType.INIT);
        expect(result.value).toBe(data);
    });

    test('createSelectAnswer', () => {
        // when
        const result = createSelectAnswer(4);

        // then
        expect(result.type).toBe(ActionType.SELECT_ANSWER);
        expect(result.value).toBe(4);
    });
});