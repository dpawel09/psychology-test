import { combineReducers, createStore } from 'redux';
import quizReducer from './QuizReducer';

interface Store {
    quiz: Quiz;
}

interface Quiz {
    questions: Question[];
    score: number;
    currentQuestionIndex: number;
    isCompleted: boolean;
}

interface Question {
    question: string;
    answers: Answer[];
}

interface Answer {
    answer: string;
    score: number;
}

const store = createStore(combineReducers({
    quiz: quizReducer,
}));

export {
    store,
    Store,
    Quiz,
    Question,
    Answer,
}